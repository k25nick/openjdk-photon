FROM openjdk:13.0.1-jdk as packager
LABEL maintainer="k25nick@gmail.com"

#1: JDK 13 with required modules
RUN jlink \
    --module-path /usr/lib/jvm/java-13-openjdk-amd64/jmods \
    --verbose \
    --add-modules java.base,java.se,jdk.security.auth,jdk.xml.dom,java.logging,java.xml,jdk.unsupported,java.sql,java.naming,java.desktop,java.management,java.security.jgss \
    --compress 2 \
    --no-header-files \
    --no-man-pages \
    --vm=server \
    --strip-debug \
    --output /opt/jdk-minimal

#2: Strip unnecessary build info from libjvm.so
FROM debian:sid as cleaner
RUN apt-get update && apt-get install -y gcc
COPY --from=packager /opt/jdk-minimal /opt/jdk-minimal
RUN strip -p --strip-unneeded /opt/jdk-minimal/lib/server/*.so
RUN strip -p --strip-unneeded /opt/jdk-minimal/lib/*.so

#3: Build the JDK11 image with minimal content
FROM photon:3.0-20191101
COPY --from=cleaner /opt/jdk-minimal /opt/jdk-minimal
RUN yum install -y libstdc++

ENV JAVA_HOME=/opt/jdk-minimal
ENV PATH="$PATH:$JAVA_HOME/bin"
